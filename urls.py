import os

from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.views.generic.base import TemplateView

urlpatterns = [
  url(r'^admin/', include(admin.site.urls)),
  url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
  url(r'^api/', include('api.urls'))
]

# In development, serve static client files.
if settings.DEBUG:
  urlpatterns += static('partials', document_root=os.path.join(settings.CLIENT_DIR, 'partials'))
  urlpatterns += static('scripts', document_root=os.path.join(settings.CLIENT_DIR, 'scripts'))
  urlpatterns += static('styles', document_root=os.path.join(settings.CLIENT_DIR, 'styles'))
  urlpatterns += [
    url(r'$', TemplateView.as_view(template_name='index.html'))
  ]
