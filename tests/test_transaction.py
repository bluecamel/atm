from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase

from api.models import Transaction


class TransactionTests(APITestCase):
    fixtures = ['users.json']

    def getAccountBalance(self):
        """
        Get the user's current account balance.
        """
        response = self.client.get(
            reverse('account-detail', args=(self.user.account.id,))
        )

        return response.data['balance']

    def setUp(self):
        """
        Setup attributes used by all tests.
        """
        self.user = User.objects.get(pk=1)
        self.pin = '123456'

        # authenticate the user and get a token
        response = self.client.post(
            reverse('api-auth'),
            {
                'account': self.user.account.id,
                'pin': self.pin
            }
        )

        # save the token from the response.
        self.token = response.data['key']

        # Set authorization header on all api calls.
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token)

    def test_transaction_delete(self):
        """
        Make sure the account owner is not allowed to delete
        a transaction.
        """
        # create a transaction to retrieve
        response = self.client.post(
            reverse('transaction-list'),
            {
                'account': self.user.account.id,
                'amount': 100.00
            }
        )

        # call transaction-detail delete
        response = self.client.delete(
            # ask for the transaction created above
            reverse('transaction-detail', args=(response.data['id'],))
        )

        # make sure we got HTTP 403
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_transaction_deposit(self):
        """
        Make sure the account owner can create a deposit transaction.
        TODO: can abstract logic shared with test_transaction_withdrawal
        """
        account = self.user.account.id
        amount = 100.00

        # get the account balance before the deposit
        balanceBefore = float(self.getAccountBalance())

        # call transaction-list create
        response = self.client.post(
            reverse('transaction-list'),
            {
                'account': account,
                'amount': amount
            }
        )

        # make sure we get a created response (HTTP 201)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # make sure the returned transaction values are correct
        self.assertEqual(response.data['account'], account)

        # this is a little awkward, and maybe the API should return a float instead of a string?
        self.assertEqual(float(response.data['amount']), amount)

        # get the account balance before the deposit
        balanceAfter = float(self.getAccountBalance())

        # make sure the balance changed the same amount as the deposit
        self.assertEqual(balanceAfter, balanceBefore + amount)

    def test_transaction_list(self):
        """
        Make sure the account owner can get a list of transactions.
        """
        transactionAmounts = [100.00, 200.00, -100.00]

        # create some transactions first
        for amount in transactionAmounts:
            self.client.post(
                reverse('transaction-list'),
                {
                    'account': self.user.account.id,
                    'amount': amount
                }
            )

        # call transaction-list retrieve
        response = self.client.get(
            reverse('transaction-list')
        )

        # make sure we got the list of transactions that we created
        for index, amount in enumerate(transactionAmounts):
            transaction = response.data[index]
            self.assertEqual(float(transaction['amount']), amount)

    def test_transaction_overdeposit(self):
        """
        Make sure the account owner can deposit more
        than their account balance.

        This is to make sure the validation logic doesn't just
        check that the amount is greater than the balance.
        """
        # call transaction-list create
        response = self.client.post(
            reverse('transaction-list'),
            {
                'account': self.user.account.id,
                'amount': abs(float(self.user.account.balance)) + 1.00
            }
        )

        # make sure we got HTTP 201
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_transaction_overdraft(self):
        """
        Make sure the account owner is not allowed to make
        a withdrawal for more than their account balance.
        """
        # call transaction-list create
        response = self.client.post(
            reverse('transaction-list'),
            {
                'account': self.user.account.id,
                'amount': -(abs(float(self.user.account.balance)) + 1.00)
            }
        )

        # make sure we got HTTP 400
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # make sure we got an error message
        self.assertEqual(response.data['non_field_errors'], ['Funds unavailable.'])

    def test_transaction_retrieve(self):
        """
        Make sure the account owner can retrieve a transaction.
        """
        account = self.user.account.id
        amount = 100.00

        # create a transaction to retrieve
        response = self.client.post(
            reverse('transaction-list'),
            {
                'account': account,
                'amount': amount
            }
        )

        # call transaction-detail retrieve
        response = self.client.get(
            # ask for the transaction created above
            reverse('transaction-detail', args=(response.data['id'],))
        )

        # make sure we get a success response
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # make sure the transaction data is correct
        self.assertEqual(response.data['account'], account)
        self.assertEqual(float(response.data['amount']), amount)

    def test_transaction_update(self):
        """
        Make sure the account owner is not allowed to update
        a transaction.
        """
        # create a transaction to retrieve
        response = self.client.post(
            reverse('transaction-list'),
            {
                'account': self.user.account.id,
                'amount': 100.00
            }
        )

        # call transaction-detail update
        response = self.client.put(
            # ask for the transaction created above
            reverse('transaction-detail', args=(response.data['id'],))
        )

        # make sure we got HTTP 403
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_transaction_withdrawal(self):
        """
        Make sure the account owner can create a withdrawal transaction.
        TODO: can abstract logic shared with test_transaction_deposit
        """
        account = self.user.account.id
        amount = -100.00

        # get the account balance before the deposit
        balanceBefore = float(self.getAccountBalance())

        # call transaction-list create
        response = self.client.post(
            reverse('transaction-list'),
            {
                'account': account,
                'amount': amount
            }
        )

        # make sure we get a created response (HTTP 201)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # make sure the returned transaction values are correct
        self.assertEqual(response.data['account'], account)

        # this is a little awkward, and maybe the API should return a float instead of a string?
        self.assertEqual(float(response.data['amount']), amount)

        # get the account balance before the deposit
        balanceAfter = float(self.getAccountBalance())

        # make sure the balance changed the same amount as the deposit
        self.assertEqual(balanceAfter, balanceBefore + amount)
