from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase

from api.models import Account


class AccountTests(APITestCase):
    fixtures = ['users.json']

    def setUp(self):
        """
        Setup attributes used by all tests.
        """
        self.user = User.objects.get(pk=1)
        self.pin = '123456'

        # authenticate the user and get a token
        response = self.client.post(
            reverse('api-auth'),
            {
                'account': self.user.account.id,
                'pin': self.pin
            }
        )

        # save the token from the response.
        self.token = response.data['key']

        # Set authorization header on all api calls.
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token)

    def test_account_retrieve(self):
        """
        Make sure the account list view returns the right data.
        """
        # call account-detail retrieve
        response = self.client.get(
            reverse('account-detail', args=(self.user.account.id,))
        )

        # make sure we get a success response
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # make sure we got the correct account data
        self.assertEqual(response.data['balance'], u'10000.00')
        self.assertEqual(response.data['id'], 1)
        self.assertEqual(response.data['user']['pk'], 1)
        self.assertEqual(response.data['user']['username'], 'perplumblewintz')
        self.assertEqual(response.data['user']['first_name'], 'Perplumblewintz')
        self.assertEqual(response.data['user']['last_name'], 'Jonz')

    def test_account_create_not_allowed(self):
        """
        Make sure creating an account is not allowed.
        """
        # call account-list create
        response = self.client.post(
            reverse('account-list'),
            {
                'pk': 2,
                'balance': 10000.00,
                'user_id': 1
            }
        )

        # make sure we got HTTP 403
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_account_delete_not_allowed(self):
        """
        Make sure deleting an account is not allowed.
        """
        # call account-list delete
        response = self.client.delete(
            reverse('account-detail', args=(self.user.account.id,))
        )

        # make sure we got HTTP 403
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_account_update_not_allowed(self):
        """
        Make sure updating an account is not allowed.
        """
        # call account-list update
        response = self.client.put(
            reverse('account-detail', args=(self.user.account.id,))
        )

        # make sure we got HTTP 403
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
