from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase


class AuthenticationTests(APITestCase):
    fixtures = ['users.json']

    def setUp(self):
        """
        Setup attributes used by all tests.
        """
        self.user = User.objects.get(pk=1)
        self.pin = '123456'
        self.url = reverse('api-auth')

    def test_auth_invalid_account(self):
        """
        Test auth with an invalid account id.
        """
        # call api-auth
        response = self.client.post(
            self.url,
            {
                'account': 2,
                'pin': '654321'
            }
        )

        # make sure we get HTTP 400 response
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_auth_invalid_pin(self):
        """
        Test auth with an invalid pin.
        """
        # call api-auth
        response = self.client.post(
            self.url,
            {
                'account': 1,
                'pin': '654321'
            }
        )

        # make sure we get HTTP 400 response
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_auth_invalid_parameters(self):
        """
        Test auth with invalid parameters.
        """
        # call api-auth
        response = self.client.post(
            self.url,
            {
                'account': 2
            }
        )

        # make sure we get HTTP 400 response
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_auth_valid(self):
        """
        Test auth with a valid account id and pin.
        """
        # call api-auth
        response = self.client.post(
            self.url,
            {
                'account': self.user.account.id,
                'pin': self.pin
            }
        )

        # make sure we get a success response
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # make sure we got the correct token
        token = Token.objects.get(user=self.user)
        self.assertEqual(response.data, {'key': token.key})
