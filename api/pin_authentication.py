from django.conf import settings
from django.contrib.auth.models import User

from passlib.hash import pbkdf2_sha256

from .models import Account


class AccountPinAuthenticationBackend(object):
    """
    Custom authentication backend to authenticate a
    user via account id and pin.
    """

    def authenticate(self, account=None, pin=None):
        """
        Authenticate using account id and pin.
        On failure, return None.
        On success, return a User object.
        """
        # Try to find an account with a matching id.
        try:
            account = Account.objects.get(pk=account)
        except Account.DoesNotExist:
            return None

        # Verify the given pin.
        if pbkdf2_sha256.verify(pin, account.pin_hash):
            return account.user
        else:
            return None

    def get_user(self, user_id):
        """
        Return a User object for the given user id.
        """
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
