from django.contrib.auth import authenticate
from django.contrib.auth.models import User

from rest_framework import exceptions, serializers
from rest_framework.authtoken.models import Token

from .models import Account, Transaction


class AuthenticationSerializer(serializers.Serializer):
    """
    Serializer for authentication.
    Handles authentication of account id and pin.
    """
    account = serializers.CharField()
    pin = serializers.CharField(min_length=6, max_length=6)

    def validate(self, attrs):
        """
        Authenticate using account id and pin.
        """
        account = attrs.get('account')
        pin = attrs.get('pin')

        # Authenticate the pin.
        user = authenticate(account=account, pin=pin)

        # Raise and exception if we didn't get a user.
        if not user:
            raise exceptions.ValidationError('Invalid pin.')

        attrs['user'] = user
        return attrs


class TokenSerializer(serializers.ModelSerializer):
    """
    Serializer for the Token model.
    """
    class Meta:
        model = Token
        fields = ('key',)


class UserSerializer(serializers.ModelSerializer):
    """
    Serializer for the User model.
    """
    class Meta:
        model = User
        fields = ('pk', 'username', 'first_name', 'last_name')


class TransactionSerializer(serializers.ModelSerializer):
    """
    Serializer for the Transaction model.
    """
    class Meta:
        model = Transaction
        # TODO: make account not required (default to current user)
        #fields = ('amount', 'timestamp')

    def validate(self, attrs):
        """
        Make sure the transaction is valid before continuing.
        """
        account = attrs.get('account')
        amount = attrs.get('amount')

        # Don't allow the transaction if it would result in a
        # negative balance.
        possibleBalance = float(amount) + float(account.balance)
        if possibleBalance < 0:
            raise exceptions.ValidationError('Funds unavailable.')

        return attrs


class AccountSerializer(serializers.ModelSerializer):
    """
    Serializer for the Account model.
    """
    user = UserSerializer(read_only=True, required=False)
    transactions = TransactionSerializer(many=True, read_only=True)

    class Meta:
        model = Account
        exclude = ('pin_hash',)
