from django.conf.urls import include, url
from django.contrib import admin

from rest_framework.routers import DefaultRouter

from .views import PinAuthenticationView, AccountViewSet, TransactionViewSet

# Create a router and register viewsets.
router = DefaultRouter()
router.register(r'account', AccountViewSet)
router.register(r'transaction', TransactionViewSet)

urlpatterns = [
    url(r'^auth/', PinAuthenticationView.as_view(), name='api-auth'),
    url(r'^', include(router.urls))
]
