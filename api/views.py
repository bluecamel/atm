from rest_framework import renderers, status, viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.decorators import detail_route
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response

from .models import Account, Transaction
from .permissions import IsAccountOwner, IsTransactionOwner
from .serializers import AuthenticationSerializer, TokenSerializer, AccountSerializer, TransactionSerializer, UserSerializer


class PinAuthenticationView(GenericAPIView):
  """
  Facilitate account/pin authentication and token response.
  """
  permission_classes = (AllowAny,)
  serializer_class = AuthenticationSerializer
  response_serializer = TokenSerializer
  token_model = Token

  def post(self, request, *args, **kwargs):
    """
    Accept authentication attempts with an account id and pin.
    On success, return a response including a token.
    """
    # Create a serializer with request data.
    self.serializer = self.get_serializer(data=self.request.data)

    # Return an error response if the serializer doesn't validate
    # the request data (i.e. invalid pin).
    if not self.serializer.is_valid():
      return Response(
        self.serializer.errors, status=status.HTTP_400_BAD_REQUEST
      )

    # Get the validated user.
    self.user = self.serializer.validated_data['user']

    # Get a token for the user.
    self.token, created = self.token_model.objects.get_or_create(
      user=self.user
    )

    # Return a response with the token.
    return Response(
      self.response_serializer(self.token).data, status=status.HTTP_200_OK
    )


class AccountViewSet(viewsets.ModelViewSet):
  """
  Facilitate detail, creation, updating, and deleting of accounts.
  """
  permission_classes = (IsAccountOwner,)
  queryset = Account.objects.all()
  serializer_class = AccountSerializer

  def get_queryset(self):
    """
    Control the accounts that can be operated on.
    """
    # Make sure an account owner can only operate on their own account.
    if self.request.user.is_superuser:
        return Account.objects.all()
    else:
        return Account.objects.filter(id=self.request.user.id)

  @detail_route(renderer_classes=[renderers.StaticHTMLRenderer])
  def highlight(self, request, *args, **kwargs):
    account = self.get_object()
    return Response(account.highlighted)

  def pre_save(self, obj):
    """Set user to current user."""
    obj.user = request.user
    return super(AccountViewSet, self).pre_save(obj)


class TransactionViewSet(viewsets.ModelViewSet):
  """
  Facilitate detail, creation, updating, and deleting of transactions.
  """
  permission_classes = (IsTransactionOwner,)
  queryset = Transaction.objects.all()
  serializer_class = TransactionSerializer

  def pre_save(self, obj):
    """Set account to current user's account."""
    obj.account = Account(user=request.user)
    return super(TransactionViewSet, self).pre_save(obj)

