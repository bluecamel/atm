from django.db import models
from django.contrib.auth.models import User


class Account(models.Model):
    """
    An account model to store a user's balance and pin hash.
    """
    # For now, assume one account per user, but this could be
    # changed to a one to many relation (i.e. models.ForeignKey).
    user = models.OneToOneField(User, related_name='account')
    balance = models.DecimalField(max_digits=17, decimal_places=2)
    pin_hash = models.CharField(max_length=237)


class Transaction(models.Model):
    """
    A transaction model for keeping track of withdrawals and deposits.
    """
    account = models.ForeignKey(Account)
    amount = models.DecimalField(max_digits=7, decimal_places=2)
    timestamp = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        """
        Update the account balance before saving the transaction.
        """
        if not self.pk:
            # Update the account balance.
            self.account.balance += self.amount
            self.account.save()
        else:
            # This should adjust the balance based on previous and new amount,
            # if updates were allowed by an administrator in the future.
            pass

        # Save the transaction by calling Model.save.
        super(Transaction, self).save(*args, **kwargs)
