from rest_framework import permissions

from .models import Account, Transaction


class IsAccountOwner(permissions.BasePermission):
    """
    Custom permission for accounts.
    """
    def has_permission(self, request, view):
        """
        Determine whether the request user has permission to
        make the request.
        """
        if request.method == u'POST' and not request.user.is_superuser:
            return False

        return True

    def has_object_permission(self, request, view, obj):
        """
        Determine whether the request user has permission to
        act on the account.
        """
        # Allow only if the request is coming from the account owner
        # and the method is read only (GET, HEAD, OPTIONS).
        if obj.user == request.user and request.method in permissions.SAFE_METHODS:
            return True

        # Deny permissions for everything else.
        return False


class IsTransactionOwner(permissions.BasePermission):
    """
    Custom permission for transactions.
    """
    def has_object_permission(self, request, view, obj):
        # Allow creation by the user, but not updates.
        if obj.account.user == request.user and request.method in (u'GET', u'POST'):
            return True

        # Deny permissions for everything else.
        return False
