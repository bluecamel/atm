"""
ATM using Django REST Framework and Angular JS.
"""

import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = '57bwxegd%#9t$urn__56=#ckv_!0r=(1&%yu-0r@7q9e0qaqz_'

# Should be False in production.
DEBUG = True

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'api.pin_authentication.AccountPinAuthenticationBackend'
)

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'corsheaders',
    'rest_framework',
    'rest_framework.authtoken',
    'api'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware'
)

ROOT_URLCONF = 'urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'atm',
        'USER': 'atm',
        'PASSWORD': 'atm',
        'HOST': '127.0.0.1',
        'PORT': '3306'
    }
}


# Internationalization
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Settings for Django REST Framework
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication'
    ),
    #'DEFAULT_PERMISSION_CLASSES': (),
    'TEST_REQUEST_DEFAULT_FORMAT': 'json'
}

# In development, allow requests from anywhere.
if DEBUG:
    CORS_ORIGIN_ALLOW_ALL = True

# Settings for serving static files in development.
if DEBUG:
    STATIC_URL = '/static/'
    CLIENT_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'client')
    STATIC_DIR = CLIENT_DIR

    TEMPLATES[0]['DIRS'] += [
        os.path.join(CLIENT_DIR)
    ]
