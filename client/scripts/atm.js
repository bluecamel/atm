'use strict';

angular.module('atm', [
   'ngRoute',
   'angular-storage',
   'atm.directives',
   'atm.services'
])
  .constant('atmConfig', {
    api: {
      baseUri: 'http://localhost:8000/api/'
    }
  })
  .config(['$httpProvider', function ($httpProvider) {
    // Inject the token into every request.
    // Ideally, this would be a service, but my Angular-fu is rusty.
    $httpProvider.interceptors.push(['store', function(store) {
      return {
        request: function(config) {
          var token = store.get('token');

          if (token) {
            config.headers.authorization = "Token " + token;
          }

          return config;
        }
      };
    }]);
  }]);
