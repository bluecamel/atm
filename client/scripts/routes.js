'use strict';

angular.module('atm')
  .config(['$routeProvider', function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'partials/home.html',
        controller: 'HomeController'
      })
      .when('/balance', {
        templateUrl: 'partials/balance.html',
        controller: 'BalanceController'
      })
      .when('/deposit', {
        templateUrl: 'partials/deposit.html',
        controller: 'DepositController'
      })
      .when('/login', {
        templateUrl: 'partials/login.html',
        controller: 'LoginController'
      })
      .when('/withdrawal', {
        templateUrl: 'partials/withdrawal.html',
        controller: 'WithdrawalController'
      })
      .otherwise({
        redirectTo: '/'
      });
  }])
  .run(['$rootScope', '$location', 'user', function($rootScope, $location, user) {
    // If not logged in, redirect to login view.
    $rootScope.$on("$routeChangeStart", function(event, next, current) {
      if (!user.loggedIn()) {
        $location.path('/login');
      }
    });
  }]);
