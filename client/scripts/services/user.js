angular.module('services.user', [])
  .factory('user', ['$http', '$location', 'atmConfig', 'store', function($http, $location, atmConfig, store) {
    var user = {
      accountId: undefined,
      token: undefined,

      getBalance: function() {
        var self = this;

        return $http({
          method: 'GET',
          url: atmConfig.api.baseUri + 'account/' + self.accountId + '/'
        });
      },

      init: function() {
        var self = this;

        // load the account id and token after browser refresh
        if (!self.accountId || !self.token) {
          self.accountId = store.get('accountId');
          self.token = store.get('token');
        }
      },

      loggedIn: function() {
        var self = this;
        return (self.accountId && self.token);
      },

      login: function(accountId, pin) {
        var self = this;

        return $http({
          method: 'POST',
          url: atmConfig.api.baseUri + 'auth/',
          data: {
            account: accountId,
            pin: pin
          }
        })
        .success(function(data, status, headers, config) {
          if (status == '200') {
            self.accountId = accountId;
            self.token = data.key;

            // store the account id and token
            store.set('accountId', self.accountId);
            store.set('token', self.token);
          }
        });
      },

      logout: function() {
        var self = this;

        // remove the account id and token from storage
        store.remove('accountId');
        store.remove('token');

        // remove the account id and token from the service
        self.accountId = undefined;
        self.token = undefined;

        $location.path('/');
      }
    }

    user.init()
    return user;
  }]);