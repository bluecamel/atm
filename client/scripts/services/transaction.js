angular.module('services.transaction', [])
  .factory('transaction', ['$http', 'atmConfig', 'user', function($http, atmConfig, user) {
    var transaction = {
      commit: function(amount) {
        return $http({
          method: 'POST',
          url: atmConfig.api.baseUri + 'transaction/',
          data: {
            account: user.accountId,
            amount: amount
          }
        });
      }
    };

    return transaction;
  }]);