'use strict';

angular.module('atm')
  .controller('WithdrawalController', ['$scope', '$location', '$timeout', 'transaction', function ($scope, $location, $timeout, transaction) {
    $scope.displayDefault = "Withdrawal amount";
    $scope.amount = "";
    $scope.display = $scope.displayDefault;

    $scope.actionCancel = function() {
      $location.path('/');
    };

    $scope.actionClear = function() {
      $scope.amount = "";
      $scope.display = $scope.displayDefault;
    };

    $scope.actionDigit = function(value) {
      if ($scope.amount == $scope.displayDefault) {
        $scope.amount = "" + value;
      } else {
        $scope.amount += value;
      }

      updateDisplay();
    };

    $scope.actionEnter = function() {
      transaction.commit(-parseFloat($scope.display))
        .success(function(data, success, headers, config) {
          $location.path('/balance');
        })
        .error(function(data, success, headers, config) {
          if (data.non_field_errors !== undefined) {
            $scope.display = data.non_field_errors[0];
            $scope.lcdFlash();
          }
        });
    };

    $scope.lcdFlash = function() {
      $scope.lcdError = true;
      $timeout(function() {
        $scope.lcdError = false;
      }, 300);
    };

    function updateDisplay() {
      var amount = $scope.amount;

      // left pad with 0's when less than 3 digits
      if (amount.length == 1) {
        amount = "00" + amount;
      } else if (amount.length == 2) {
        amount = "0" + amount;
      }

      // format so that last 2 digits are decimals
      var amountFormatted = amount.slice(0, amount.length - 2) + "." + amount.slice(amount.length - 2, amount.length);

      $scope.display = amountFormatted;
    }
  }]);
