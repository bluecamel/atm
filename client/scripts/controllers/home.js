'use strict';

angular.module('atm')
  .controller('HomeController', ['$scope', '$location', 'user', function($scope, $location, user) {
    $scope.menuItems = new Array(
      "balance",
      "deposit",
      "withdrawal",
      "logout"
    );

    $scope.select = function(item) {
      if (item == 'logout') {
        // handle logout as a special case, since we don't need a view
        user.logout();
        $location.path('/authentication');
      } else {
        $location.path("/" + item);
      }
    };
  }]);
