'use strict';

angular.module('atm')
  .controller('LoginController', ['$scope', '$timeout', '$location', 'user', function ($scope, $timeout, $location, user) {
    // Setup input buffer and input variables.
    $scope.inputBuffer = "";
    $scope.accountId = undefined;
    $scope.pin = undefined;

    // Define max number of account id and pin digits.
    $scope.maxDigitsAccountId = 10;
    $scope.maxDigitsPin = 6;

    // Define default display values depending on state.
    $scope.lcdDefaultAccountId = "Enter your account id";
    $scope.lcdDefaultPin = "Enter your " + $scope.maxDigitsPin + "-digit pin";

    // LCD states.
    $scope.display = "";
    $scope.lcdError = false;

    // Clear the display and redirect to home.
    $scope.actionCancel = function(value) {
      $scope.accountId = undefined;
      $scope.pin = undefined;
      $scope.inputBuffer = "";
      $scope.actionClear()
      updateDisplay();
    }

    // Clear the input buffer and update the LCD.
    $scope.actionClear = function(value) {
      $scope.inputBuffer = "";
      updateDisplay();
    }

    // Process digit press and udpate display.
    $scope.actionDigit = function(value) {
      if (inputBufferFull()) {  // we've hit the max digits
        $scope.lcdFlash();
      } else {
        $scope.inputBuffer += value;
      }

      updateDisplay();
    };

    // Save the input buffer and update display.
    // If both values have been entered, attempt to login.
    $scope.actionEnter = function(value) {
      if ($scope.accountId == null) {
        $scope.accountId = $scope.inputBuffer;
        $scope.actionClear();
      } else {
        $scope.pin = $scope.inputBuffer;
        $scope.login();
      }

      updateDisplay();
    }

    // Flash the display red when there's an error.
    $scope.lcdFlash = function() {
      $scope.lcdError = true;
      $timeout(function() {
        $scope.lcdError = false;
      }, 300);
    };

    // Attempt to login with the given
    // account id and pin.
    $scope.login = function() {
      user.login($scope.accountId, $scope.pin)
        .success(function() {
          $location.path('/');
        })
        .error(function() {
          $scope.lcdFlash();
          $scope.actionCancel();
        });
    };

    // Check to see if we've hit the maximum digits.
    function inputBufferFull() {
      if ($scope.accountId === undefined) {
        return $scope.inputBuffer.length >= $scope.maxDigitsAccountId;
      } else if ($scope.pin === undefined) {
        return $scope.inputBuffer.length >= $scope.maxDigitsPin;
      }

      return false;
    };

    // Mask the entered value.
    function maskDigits(value) {
      var maskedValue = "";

      // Display asterisks in place of all digits, except the last.
      for (var i = 0; i < $scope.inputBuffer.length - 1; i++) {
        maskedValue += "*";
      }

      // Display the last digit.
      maskedValue += $scope.inputBuffer.substr($scope.inputBuffer.length - 1, $scope.inputBuffer.length);

      return maskedValue;
    };

    // Dynamically update the LCD display based on current state
    // and entered values.
    function updateDisplay() {
      if ($scope.accountId === undefined) {
        if ($scope.inputBuffer.length > 0) {
          $scope.display = maskDigits($scope.inputBuffer);
        } else {
          $scope.display = $scope.lcdDefaultAccountId;
        }
      } else {
        if ($scope.inputBuffer.length > 0) {
          $scope.display = maskDigits($scope.inputBuffer);
        } else {
          $scope.display = $scope.lcdDefaultPin;
        }
      }
    };

    updateDisplay();
  }]);
