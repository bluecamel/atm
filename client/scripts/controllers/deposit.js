'use strict';

angular.module('atm')
  .controller('DepositController', ['$scope', '$location', 'transaction', function ($scope, $location, transaction) {
    $scope.displayDefault = "Deposit amount";
    $scope.amount = "";
    $scope.amountDollars = $scope.displayDefault;

    $scope.actionCancel = function() {
      $location.path('/');
    };

    $scope.actionClear = function() {
      $scope.amount = "";
      $scope.amountDollars = $scope.displayDefault;
    };

    $scope.actionDigit = function(value) {
      if ($scope.amount == $scope.displayDefault) {
        $scope.amount = "" + value;
      } else {
        $scope.amount += value;
      }

      updateAmountDollars();
    };

    $scope.actionEnter = function() {
      transaction.commit(parseFloat($scope.amountDollars))
        .success(function(data, success, headers, config) {
          $location.path('/balance');
        })
        .error(function(data, success, headers, config) {
          console.log('error', data, success, headers, config);
        });  // TODO: error handling
    };

    $scope.lcdFlash = function() {
      $scope.lcdError = true;
      $timeout(function() {
        $scope.lcdError = false;
      }, 300);
    };

    function updateAmountDollars() {
      var amount = $scope.amount;

      // left pad with 0's when less than 3 digits
      if (amount.length == 1) {
        amount = "00" + amount;
      } else if (amount.length == 2) {
        amount = "0" + amount;
      }

      // format so that last 2 digits are decimals
      var amountFormatted = amount.slice(0, amount.length - 2) + "." + amount.slice(amount.length - 2, amount.length);

      $scope.amountDollars = amountFormatted;
    }
  }]);
