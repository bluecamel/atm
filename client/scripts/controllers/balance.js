'use strict';

angular.module('atm')
  .controller('BalanceController', ['$scope', '$http', '$location', 'user', function($scope, $http, $location, user) {
    user.getBalance()
      .success(function(data) {
        $scope.balance = "$" + data.balance;
      });
  }]);
