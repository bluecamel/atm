'use strict';

/*
 *  Keypad directive module.
 */
angular.module('directives.keypad', [])
  .controller('keypadController', function($scope) {
    // Define action buttons and bind to methods.
    $scope.actionButtons = {
      "cancel": $scope.actionCancel,
      "clear": $scope.actionClear,
      "enter": $scope.actionEnter
    };

    // Define the digit button layout.
    // Can be overridden for a different layout.
    $scope.digitButtonRows = new Array(
      new Array({"value": 1}, {"value": 2}, {"value": 3}),
      new Array({"value": 4}, {"value": 5}, {"value": 6}),
      new Array({"value": 7}, {"value": 8}, {"value": 9}),
      new Array({"value": undefined}, {"value": 0}, {"value": undefined})
    );

    $scope.pressActionButton = function(value) {
      $scope.actionButtons[value]({value: value});
    };

    $scope.pressDigitButton = function(value) {
      if (value !== undefined) {  // ignore disabled buttons
        $scope.actionDigit({value: value});
      }
    };
  })
  .directive('atmKeypad', function() {
    return {
      controller: 'keypadController',
      scope: {
        actionCancel: '&',
        actionClear: '&',
        actionEnter: '&',
        actionDigit: '&'
      },
      templateUrl: 'partials/directives/keypad.html'
    };
  });
