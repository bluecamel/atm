'use strict';

/*
 *  LCD display directive module.
 */
angular.module('directives.lcd', [])
  .directive('atmLcd', function() {
    return {
      scope: {
        display: '=',
        error: '='
      },
      templateUrl: 'partials/directives/lcd.html'
    };
  });
