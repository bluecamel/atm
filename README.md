# Totally Real ATM

## Installation instructions (OS X)

#### Install pip and virtualenv
    sudo easy_install pip
    sudo pip install virtualenv

#### Checkout the repo
    git clone git@bitbucket.org:bluecamel/atm.git

#### Create a MySQL database
    CREATE DATABASE `atm`;
    GRANT ALL PRIVILEGES ON `atm`.* TO `atm`@`%` IDENTIFIED BY 'atm';

#### CD into the repo and create a virtual environment
    cd atm
    virtualenv virtual
    source virtual/bin/activate

#### Install the app dependencies
    pip install -r requirements.txt

#### Create database tables
    python manage.py syncdb

**After creating the tables, this will prompt you to create a superuser.  Doing so will create an account that will allow you to login to the admin panel.**


#### Load initial user and account data.
    python manage.py loaddata users.json

#### Start the development server
    python manage.py runserver

#### Run the python tests
    python runtests.py

#### Play!
1. [ATM (http://127.0.0.1:8000/)](http://127.0.0.1:8000/)
2. [Admin (http://127.0.0.1:8000/admin/)](http://127.0.0.1:8000/admin/)
3. [API documentation (http://127.0.0.1:8000/api/)](http://127.0.0.1:8000/api/)

#### Exit the virtualenv
    deactivate
